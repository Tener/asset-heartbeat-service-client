#
# Configuration defaults for the heartbeat service client.
#

import winreg

class Config:
    # Class variable are defaults. Otherwise, @property methods should return
    # the Windows registry setting. If dev == True, return dev constants.

    # Service names
    SVC_NAME = "AssetHeartbeatServiceClient"
    SVC_DISPLAY_NAME = 'Asset Heartbeat Service Client'
    SVC_DESCRIPTION = 'Asset Heartbeat Emitter for Windows'

    # URI of heartbeat server. The path should specify the "assets" route.
    # eg. 'https://heartbeat-server.local/assets'
    HEARTBEAT_URI = "http://localhost:3000/assets"

    # Number of POST requests to send per minute (0 < x <= 60).
    # eg. A setting of 30 will send a heartbeat every other second.
    INTERVAL_BPM = 1

    # Registry hive and path for the service
    REG_HIVE = winreg.HKEY_LOCAL_MACHINE
    REG_PATH = 'SYSTEM\\CurrentControlSet\\Services\\' + SVC_NAME

    def __init__(self, dev=False):
        self._heartbeat_uri = Config.HEARTBEAT_URI
        self._dev_mode = dev

        if Config.INTERVAL_BPM < 1:
            self._interval = 1
        elif Config.INTERVAL_BPM > 60:
            self._interval = 60
        else:
            self._interval = round(60 / Config.INTERVAL_BPM)

    @property
    def heartbeat_uri(self):
        if self.dev_mode:
            return 'http://localhost:3000/assets'

        reg_value = Config.get_reg('HeartbeatURI')
        if reg_value is not None:
            return reg_value
        else:
            Config.set_reg('HeartbeatURI', self.HEARTBEAT_URI)
            return self._heartbeat_uri

    @property
    def interval(self):
        return 10

        # FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME
        # none of this shit works
        if self.dev_mode:
            return 60

        reg_value = Config.get_reg('HeartbeatsPerMinute')
        if reg_value is not None:
            return round(60 / int(reg_value)) # FIXME: make a setter for self._interval; DRY
        else:
            Config.set_reg('HeatbeatsPerMinute', str(self.INTERVAL_BPM, 'utf8'))
            return self._interval

    @property
    def dev_mode(self):
        return self._dev_mode

    @staticmethod
    def get_reg(name):
        try:
            with winreg.OpenKey(Config.REG_HIVE, Config.REG_PATH, 0, winreg.KEY_READ) as key:
                value, _ = winreg.QueryValueEx(key, name)
            return value
        except WindowsError:
            return None

    @staticmethod
    def set_reg(name, value):
        try:
            winreg.CreateKey(Config.REG_HIVE, Config.REG_PATH)
            with winreg.OpenKey(Config.REG_HIVE, Config.REG_PATH, 0, winreg.KEY_WRITE) as key:
                winreg.SetValueEx(key, name, 0, winreg.REG_SZ, value)
            return True
        except WindowsError:
            return False
