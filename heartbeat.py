#!/usr/bin/env python3

import re
import platform
import requests
import netifaces
import win32net

from config import Config
from devlog import console

class Heartbeat():
    def __init__(self, config):
        self.config = config

    def send(self):
        # Build object to send
        heartbeat_data = {}
        heartbeat_data['hostname'] = platform.node()
        heartbeat_data['mac_list'] = self._get_mac_list()
        heartbeat_data['ip_list'] = self._get_ipv4_list()
        heartbeat_data['users_list'] = self._get_users_list()

        # Send object
        console.log('Sending POST request...', end=' ')
        try:
            ret = requests.post(self.config.heartbeat_uri, json=heartbeat_data)
        except requests.ConnectionError:
            console.log('ConnectionError', time=False)
            return

        # Log result
        console.log(str(ret.content, 'utf8'), time=False)

    def _get_ipv4_list(self):
        """
        Return a list of IPv4 addresses associated with the host machine.
        """
        addr_list = [netifaces.ifaddresses(iface)[netifaces.AF_INET][0]['addr'] for iface in netifaces.interfaces() if netifaces.AF_INET in netifaces.ifaddresses(iface)]
        return [ip for ip in addr_list if not re.match(r'(127\..*\..*\..*)|(169\.254\..*\..*)', ip)]

    def _get_mac_list(self):
        """
        Return a list of MAC addresses associated with the host machine.
        """
        addr_list = [netifaces.ifaddresses(iface)[netifaces.AF_LINK][0]['addr'] for iface in netifaces.interfaces() if netifaces.AF_INET in netifaces.ifaddresses(iface)]
        return [mac for mac in addr_list if len(mac) > 0]

    def _get_users_list(self):
        """
        Return a list of user accounts on the host machine with default
        accounts filtered out.

        WDAGUtilityAccount is a new Windows 10 user for Windows Defender. Note
        that this should be updated later to filter based on whether an account
        is enabled and active, instead of only by its name.
        """
        name_dict = win32net.NetGroupGetUsers(None, 'none', 0)[0]
        return [name['name'] for name in list(filter(lambda n: not re.match(r'Administrator|Guest|DefaultAccount|WDAGUtilityAccount', n['name']), name_dict))]

if __name__ == '__main__':
    Heartbeat(Config(dev=True)).send()
