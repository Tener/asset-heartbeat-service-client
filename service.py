#!/usr/bin/env python3

# Basic template here:
# https://www.codeproject.com/Articles/1115336/Using-Python-to-Make-a-Windows-Service

import sys
import socket
import servicemanager
import win32event
import win32service
import win32serviceutil
import time

from heartbeat import Heartbeat
from config import Config

class AssetHeartbeatService(win32serviceutil.ServiceFramework):
    _svc_name_ = Config().SVC_NAME
    _svc_display_name_ = Config().SVC_DISPLAY_NAME
    _svc_description_ = Config().SVC_DESCRIPTION

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)
        self.is_started = True
        self.config = Config()

    def SvcStop(self):
        self.is_started = False
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)

    def SvcDoRun(self):
        self.is_started = True
        self.main()
        win32event.WaitForSingleObject(self.hWaitStop, win32event.INFINITE)

    def main(self):
        while self.is_started:
            Heartbeat(self.config).send()
            time.sleep(self.config.interval)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        servicemanager.Initialize()
        servicemanager.PrepareToHostSingle(AssetHeartbeatService)
        servicemanager.StartServiceCtrlDispatcher()
    else:
        win32serviceutil.HandleCommandLine(AssetHeartbeatService)
