#
# Print to console and/or event log
#

import datetime

class event:
    # Base class for logging to the event log
    @staticmethod
    def log(*args, **kwargs):
        pass

class console:
    # Base class for logging to the console
    @staticmethod
    def log(*args, dev=True, time=True, **kwargs):
        if not dev:
            return
        if time:
            print('[' + console._time_str() + ']', end=' ', flush=True)
        print(*args, **kwargs, flush=True)

    @staticmethod
    def _time_str():
        n = datetime.datetime.now()
        return datetime.datetime.strftime(n, '%H:%M:%S')
